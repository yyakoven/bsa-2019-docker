<?php
	$joke = json_decode(file_get_contents(
		"https://official-joke-api.appspot.com/jokes/programming/random"))
		[0];
	$setup = $joke->setup;
	$punchline = $joke->punchline;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Not quite</title>
	<style>
		body {
			margin: 0;
			display: flex;
			flex-direction: column;
			widows: 100vw;
			height: 100vh;
			justify-content: center;
			align-items: center;
			font-family: 'Courier New', Courier, monospace;
		}
		p {
			max-width:80%;
		}
		h1 {
			justify-self: flex-start;
		}
	</style>
</head>
<body>
	<h1>404 | Page Not Found</h1>
	<p style="color:lightgray;font-style:italic;">In the meantime here is a joke for you</p>
	<p><?= $setup ?></p>
	<p><?= $punchline ?></p>
</body>
</html>